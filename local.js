__entryscape_config = {
    staticBuildVersion: "1.2.3",
    theme: {
        appName: "", //Part of logo
        logoFileName: "logo.png",
        localTheme: true,
        showModuleNameInHeader: false
    },
    itemstore: {
        bundles: [
            "templates/dc/dc",
            "templates/dcterms/dcterms",
            "templates/rdfs/rdfs",
            "templates/schema.org/schema",
            "theme/raa"
        ]
    },
    entrystore: {
        repository: "https://raa.entryscape.net/store/"
    },
    site: {
        "!moduleList": ["terms", "workbench", "catalog", "admin"]
    },
    entrychooser: {
        "http://purl.org/dc/terms/subject": "repository"
    },
    entitytypes: {
      "museum": {
        label: { en: "Museum", sv: "Museum" },
        rdfType: "http://schema.org/Museum",
        template: "raa:Museum",
        includeInternal: true,
        inlineCreation: true,
        templateLevel: "recommended"
      },
    },
    rdf: {
        namespaces: {
            "gn": "http://www.geonames.org/ontology#",
            "schema": "http://schema.org/"
        },
        labelProperties: [
            "schema:name",
            "foaf:name",
            ["foaf:firstName", "foaf:lastName"],
            ["foaf:givenName", "foaf:familyName"],
            "vcard:fn",
            "skos:prefLabel",
            "dcterms:title",
            "dc:title",
            "rdfs:label",
            "skos:altLabel",
            "vcard:hasEmail",
            "gn:name",
            "schema:caption",
            "schema:contentURL",
            "dcterms:identifier",
            "schema:address",
            ["schema:streetAddress", "schema:addressLocality", "schema:addressRegion"],
            "land:nationalCadastralReference",
            "land:id"
        ]
    },
    reCaptchaSiteKey: "6LeraBITAAAAAETQ_-wpGZOJ7a9jKRpF1g8OYc2O"
};
