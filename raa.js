define({
  "templates": [
    {
      "type": "group",
      "id": "raa:Museum",
      "nodetype": "RESOURCE",
      "label": {
        "en": "Museum",
        "sv": "Museum"
      },
      "constraints": {
        "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://schema.org/Museum"
      },
      "items": [
        {
          "type": "text",
          "id": "raa:name",
          "nodetype": "LANGUAGE_LITERAL",
          "property": "http://schema.org/name",
          "label": {
            "en": "Name",
            "sv": "Namn"
          },
          "cardinality": {
            "min": 1
          }
        },
        {
          "id": "raa:description",
          "label": {
            "sv": "Organisationsbeskrivning",
          },
          "description": {
            "sv": "En organisations",
          },
          "cardinality": {
            "pref": 1
          },
          "extends": "dcterms:description"
        }
      ]
    },
     {
      "type": "group",
      "id": "raa:Plats",
      "nodetype": "RESOURCE",
      "label": {
        "en": "Place",
        "sv": "Plats"
      },
      "constraints": {
      //  "http://www.w3.org/1999/02/22-rdf-syntax-ns#type": "http://schema.org/Museum"
      },
      "items": [
        {
          "type": "text",
          "id": "raa:name",
          "nodetype": "LANGUAGE_LITERAL",
          "property": "http://schema.org/name",
          "label": {
            "en": "Name",
            "sv": "Namn"
          },
          "cardinality": {
            "min": 1
          }
        },
        {
          "id": "raa:xcoord",
          "label": {
            "sv": "X-koordinat",
            "en": "X-coordinate"
          },
          "description": {
            "sv": "X-Koordinat f�r platsen",
          },
          "cardinality": {
            "min": 1
          }
        },
        {
          "id": "raa:ycoord",
          "label": {
            "sv": "Y-koordinat",
          },
          "description": {
            "sv": "Y-Koordinat f�r platsen",
          },
          "cardinality": {
            "min": 1
          }
        }
      ]
    }
  ]
});

